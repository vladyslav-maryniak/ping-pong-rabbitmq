﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;
using System.Threading;

namespace RabbitMQ.Pinger
{
    public class QueueService : IDisposable
    {
        private readonly IMessageProducerScope messageProducerScope;
        private readonly IMessageConsumerScope messageConsumerScope;
        private readonly EventHandler<BasicDeliverEventArgs> outputEventHandler;

        public QueueService(
            IMessageProducerScopeFactory messageProducerScopeFactory,
            IMessageConsumerScopeFactory messageConsumerScopeFactory,
            EventHandler<BasicDeliverEventArgs> outputEventHandler
            )
        {
            messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "PingPong",
                ExchangeType = ExchangeType.Direct,
                QueueName = "ping_queue",
                RoutingKey = "ping"
            });

            messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSettings
            {
                ExchangeName = "PingPong",
                ExchangeType = ExchangeType.Direct,
                QueueName = "pong_queue",
                RoutingKey = "pong"
            });
            
            this.outputEventHandler = outputEventHandler;
            
            messageConsumerScope.MessageConsumer.Received += outputEventHandler;
            messageConsumerScope.MessageConsumer.Received += MessageConsumer_Received;
        }

        private void MessageConsumer_Received(object sender, BasicDeliverEventArgs e)
        {
            Thread.Sleep(2500);
            messageProducerScope.MessageProducer.SendMessageToQueue("ping");
            messageConsumerScope.MessageConsumer.SetAcknowledge(e.DeliveryTag, true);
        }

        public void Dispose()
        {
            messageConsumerScope.MessageConsumer.Received -= outputEventHandler;
            messageConsumerScope.MessageConsumer.Received -= MessageConsumer_Received;
            
            messageProducerScope.Dispose();
            messageConsumerScope.Dispose();
        }
    }
}
