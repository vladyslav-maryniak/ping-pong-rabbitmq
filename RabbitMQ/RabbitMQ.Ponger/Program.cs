﻿using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.QueueServices;
using System;
using System.Text;

namespace RabbitMQ.Ponger
{
    class Program
    {
        private static string title = @"  ____                             
 |  _ \ ___  _ __   __ _  ___ _ __ 
 | |_) / _ \| '_ \ / _` |/ _ \ '__|
 |  __/ (_) | | | | (_| |  __/ |   
 |_|   \___/|_| |_|\__, |\___|_|   
                   |___/
";

        private static void Main()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile($"appsettings.json", true, true)
                .Build();

            Console.WriteLine(title);

            using (var queueService = new QueueService(
                new MessageProducerScopeFactory(new ConnectionFactory() { HostName = config["HostName"] }),
                new MessageConsumerScopeFactory(new ConnectionFactory() { HostName = config["HostName"] }),
                MessageConsumer_Received))
            {
                Console.WriteLine("Press [Enter] to exit.");
                
                queueService.Send();

                Console.ReadLine();
            }
        }

        private static void MessageConsumer_Received(object sender, BasicDeliverEventArgs e)
        {
            Console.WriteLine($"[{DateTime.Now}]: {Encoding.UTF8.GetString(e.Body.ToArray())}");
        }
    }
}
