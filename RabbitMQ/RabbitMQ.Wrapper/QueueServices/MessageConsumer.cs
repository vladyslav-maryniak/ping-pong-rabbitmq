﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Models;
using System;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageConsumer : IMessageConsumer
    {
        private readonly MessageConsumerSettings settings;
        private readonly EventingBasicConsumer consumer;

        public event EventHandler<BasicDeliverEventArgs> Received
        {
            add => consumer.Received += value;
            remove => consumer.Received -= value;
        }

        public MessageConsumer(MessageConsumerSettings messageConsumerSettings)
        {
            settings = messageConsumerSettings;
            consumer = new EventingBasicConsumer(settings.Channel);
        }

        public void ListenQueue()
        {
            if (settings.SequentialFetch)
            {
                settings.Channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
            }

            settings.Channel.BasicConsume(settings.QueueName, settings.AutoAcknowledge, consumer);
        }

        public void SetAcknowledge(ulong deliveryTag, bool processed)
        {
            if (processed)
            {
                settings.Channel.BasicAck(deliveryTag, multiple: false);
            }
            else
            {
                settings.Channel.BasicNack(deliveryTag, multiple: false, requeue: false);
            }
        }
    }
}
