﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageProducerScope : IMessageProducerScope
    {
        private readonly Lazy<IMessageQueue> messageQueue;
        private readonly Lazy<IMessageProducer> messageProducer;

        private readonly MessageScopeSettings messageScopeSettings;
        private readonly IConnectionFactory connectionFactory;

        private IMessageQueue MessageQueue => messageQueue.Value;
        public IMessageProducer MessageProducer => messageProducer.Value;

        public MessageProducerScope(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings)
        {
            this.connectionFactory = connectionFactory;
            this.messageScopeSettings = messageScopeSettings;

            messageQueue = new Lazy<IMessageQueue>(CreateMessageQueue);
            messageProducer = new Lazy<IMessageProducer>(CreateMessageProducer);
        }

        private IMessageQueue CreateMessageQueue()
        {
            return new MessageQueue(connectionFactory, messageScopeSettings);
        }

        private IMessageProducer CreateMessageProducer()
        {
            return new MessageProducer(new MessageProducerSettings
            {
                Channel = MessageQueue.Channel,
                PublicationAddress = new PublicationAddress(
                    messageScopeSettings.ExchangeType,
                    messageScopeSettings.ExchangeName,
                    messageScopeSettings.RoutingKey)
            });
        }

        public void Dispose()
        {
            MessageQueue?.Dispose();
        }
    }
}