﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageQueue : IMessageQueue
    {
        private readonly IConnection connection;

        public IModel Channel { get; protected set; }

        public MessageQueue(IConnectionFactory connectionFactory)
        {
            connection = connectionFactory.CreateConnection();
            Channel = connection.CreateModel();
        }

        public MessageQueue(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings)
            : this(connectionFactory)
        {
            DeclareExchange(messageScopeSettings.ExchangeName, messageScopeSettings.ExchangeType);

            if (messageScopeSettings.QueueName != null)
            {
                BindQueue(messageScopeSettings.QueueName,
                          messageScopeSettings.ExchangeName,
                          messageScopeSettings.RoutingKey);
            }
        }

        public void DeclareExchange(string exchangeName, string exchangeType)
        {
            Channel.ExchangeDeclare(exchangeName, exchangeType ?? string.Empty);
        }

        public void BindQueue(string queueName, string exchangeName, string routingKey)
        {
            Channel.QueueDeclare(queueName,
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false);
            
            Channel.QueueBind(queueName, exchangeName, routingKey);
        }

        public void Dispose()
        {
            Channel?.Close();
            Channel?.Dispose();

            connection?.Close();
            connection?.Dispose();
        }
    }
}
