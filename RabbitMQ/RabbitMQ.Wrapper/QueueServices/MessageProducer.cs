﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System.Text;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageProducer : IMessageProducer
    {
        private readonly MessageProducerSettings messageProducerSettings;
        private readonly IBasicProperties basicProperties;

        public MessageProducer(MessageProducerSettings messageProducerSettings)
        {
            this.messageProducerSettings = messageProducerSettings;

            basicProperties = this.messageProducerSettings.Channel.CreateBasicProperties();
            basicProperties.Persistent = true;
        }

        public void SendMessageToQueue(string message, string type = null)
        {
            if (!string.IsNullOrEmpty(type))
            {
                basicProperties.Type = type;
            }

            var body = Encoding.UTF8.GetBytes(message);
            messageProducerSettings.Channel.BasicPublish(messageProducerSettings.PublicationAddress, basicProperties, body);
        }
    }
}
