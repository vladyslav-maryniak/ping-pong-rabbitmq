﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageConsumerScope : IMessageConsumerScope
    {
        private readonly Lazy<IMessageQueue> messageQueue;
        private readonly Lazy<IMessageConsumer> messageConsumer;

        private readonly MessageScopeSettings messageScopeSettings;
        private readonly IConnectionFactory connectionFactory;

        private IMessageQueue MessageQueue => messageQueue.Value;
        public IMessageConsumer MessageConsumer => messageConsumer.Value;

        public MessageConsumerScope(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings)
        {
            this.connectionFactory = connectionFactory;
            this.messageScopeSettings = messageScopeSettings;

            messageQueue = new Lazy<IMessageQueue>(CreateMessageQueue);
            messageConsumer = new Lazy<IMessageConsumer>(CreateMessageConsumer);
        }

        private IMessageQueue CreateMessageQueue()
        {
            return new MessageQueue(connectionFactory, messageScopeSettings);
        }

        private IMessageConsumer CreateMessageConsumer()
        {
            return new MessageConsumer(new MessageConsumerSettings
            {
                Channel = MessageQueue.Channel,
                QueueName = messageScopeSettings.QueueName
            });
        }

        public void Dispose()
        {
            MessageQueue?.Dispose();
        }
    }
}