﻿namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageProducer
    {
        void SendMessageToQueue(string message, string type = null);
    }
}
