﻿using RabbitMQ.Client.Events;
using System;

namespace RabbitMQ.Wrapper
{
    public interface IMessageConsumer
    {
        event EventHandler<BasicDeliverEventArgs> Received;
        
        void ListenQueue();

        public void SetAcknowledge(ulong deliveryTag, bool processed);
    }
}
