﻿using RabbitMQ.Client;

namespace RabbitMQ.Wrapper.Models
{
    public class MessageProducerSettings
    {
        public IModel Channel { get; set; }

        public PublicationAddress PublicationAddress { get; set; }
    }
}
